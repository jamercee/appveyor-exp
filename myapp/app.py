#!/usr/bin/env python2.7
# vim: set fileencoding=utf-8
# pylint:disable=line-too-long
r""":mod:`app` - short module description
######################################

.. module:: app
   :synopsis: short module description
.. moduleauthor:: Jim Carroll <jim@carroll.net>

Provide a detailed summary of the module. Break down each subsystem and
describe it's operation in detail

Command line options
********************

*usage:* ``app  [-?] [-d] ...``

Positional arguments
====================

.. option:: XXX

   Option XXX provides ...

Optional argument:
==================

.. option:: ?, -h, --help

   Display help and exit

.. option:: -d, --debug

   Generate diagnotic logging.

..
   Copyright(c) 2016, Carroll-Net, Inc., All Rights Reserved"""
# pylint:enable=line-too-long
# ----------------------------------------------------------------------------
# Standard library imports
# ----------------------------------------------------------------------------
import logging
import sys

# ----------------------------------------------------------------------------
# Module level initializations
# ----------------------------------------------------------------------------
__version__ = '1.0.1'
__author__ = 'Jim Carroll'
__email__ = 'jim@carroll.net'
__status__ = '(Development | Production | Testing | Migration)'
__copyright__ = 'Copyright(c) 2016, Carroll-Net, Inc., All Rights Reserved'

LOG = logging.getLogger('app')


def main():
    r"""main process driver"""
    pass

if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        print('CTRL-C')
        sys.exit(0)
