r"""Unittest initialization for myapp."""
import sys
import os
PTH = os.path.abspath('../myapp')
if PTH not in sys.path:
    sys.path.insert(0, PTH)
