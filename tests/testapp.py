#!/usr/bin/env python2.7
# vim: set fileencoding=utf-8
# pylint:disable=line-too-long
r""":mod:`testapp` - unittests for app
##########################################

.. module:: testapp
   :synopsis: short module description
.. moduleauthor:: Jim Carroll <jim@carroll.net>

Comprehensive unittests for app

..
   Copyright(c) 2016, Carroll-Net, Inc., All Rights Reserved"""
# pylint:enable=line-too-long
# ----------------------------------------------------------------------------
# Standard library imports
# ----------------------------------------------------------------------------
import logging
import unittest

# ----------------------------------------------------------------------------
# Module level initializations
# ----------------------------------------------------------------------------
__version__ = '1.0.1'
__author__ = 'Jim Carroll'
__email__ = 'jim@carroll.net'
__status__ = 'Testing'
__copyright__ = 'Copyright(c) 2016, Carroll-Net, Inc., All Rights Reserved'

LOG = logging.getLogger('testapp')

# pylint: disable=wrong-import-position
# pylint: disable=wrong-import-order
import myapp


class Testapp(unittest.TestCase):
    r"""app unittest test case"""

    # pylint: disable=invalid-name

    def setUp(self):
        r"""initialize test fixture"""
        pass

    def tearDown(self):
        r"""tear down test fixture"""
        pass

    def test_version(self):
        r"""confirm version exists"""
        self.assertTrue(myapp, '__version__')


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
